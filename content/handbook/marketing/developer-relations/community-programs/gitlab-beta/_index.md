---

title: "Beta Program"
description: "Learn about the GitLab Beta Program from GitLab's Contributor success team"
---

## About

This program is a part of the [Community Programs](https://handbook.gitlab.com/job-families/marketing/community-programs/) team.

GitLab's Beta program is a dynamic initiative designed to bring together a diverse community of users and gather valuable insights on upcoming features. Our goals revolve around fostering collaboration, expanding our contributor base, and ensuring that GitLab continues to evolve with the input of users from various backgrounds.

## How to reach us

* DRI: [@nick_vh](https://gitlab.com/nick_vh)
* Email: `nveenhof@gitlab.com`
* Slack channel: [#developer-relations-beta-program](https://gitlab.slack.com/messages/developer-relations-beta-program)

## Vision and goals

Our vision is to establish a convenient flow where contributors play a pivotal role in shaping the future of GitLab. The Beta program is not just a preview; it's a collaborative journey where your feedback drives innovation.

Our goals are:

1. **Community Expansion** - we aim to grow our community of contributors by actively inviting users with diverse skill sets, experiences, and perspectives. Your unique insights will contribute to the strength and versatility of the GitLab platform.

1. **Feedback Excellence** - the Beta program is a conduit for meaningful feedback. We want to hear your thoughts, suggestions, and experiences to ensure that GitLab meets and exceeds your expectations. Your input is invaluable in refining our features.

1. **Collaborative Enhancement** - this program is not just about testing. It's a collaborative effort to enhance GitLab's capabilities. Engage in discussions, share ideas, and collaborate with fellow Beta members to drive innovation together.

## How does it work?

We are creating collaborative workflow between GitLab engineering groups and open-source contributors. The initial cooperation is reflected on next diagram:

![Use cases diagram image](/images/handbook/marketing/developer-relations/community-programs/gitlab-beta/use-cases-diagram.png "Use cases diagram")

1. **Sign up** - contributor follows link from marketing campaign to [landing page](https://about.gitlab.com/community/beta) and submits the form. Our team receives user data for supporting, informing and awarding purposes.
   During sign up our contributors fill-in Group ID which they will use for Beta testing (sandbox group).

1. **Grant license** - features in Beta aren't free tier only. After sign up contributor receives email where and how to request trial Ultimate license. Each contributor requests license with already existing way as it works for [GitLab Heroes program](https://handbook.gitlab.com/handbook/marketing/developer-relations/evangelist-program/#gitlab-heroes-licenses).

1. **Put features into Beta program** - our team always keep hand on a pulse of new features that appear among engineering groups week-by-week.
So, once we see some potential candidate that looks appropriate for Beta program then we contact PM from this engineering group.
But we are also glad to receive feature candidates from engineering groups initiative. We are wide open to hear about ongoing feature candidates in [slack channel](https://gitlab.slack.com/messages/developer-relations-beta-program) or in thread of [this issue](https://gitlab.com/gitlab-org/developer-relations/contributor-success/team-task/-/issues/390).
As final approval step we request review from a product for merge request where we add feature into Beta.

1. **Gather feedback** - engineering groups review comments under their feedback issues. Our team at this point mostly track feedback activity per each Beta member. After this step engineering group can decide to put it out from Beta if they want to. Usual lifetime of feature inside Beta starts from 4-5 weeks but we don't mind to keep it for longer.

1. **License prolongation** - depending on our tracking results - if contributor is active (leaves feedback) then we (contributor success team) request license prolongation (1 more month) for this individual member through support form.

1. **Awarding** - after 2-3 month after Beta program launch we will start to nominate feedback contributors by some criteria. The exact criteria & prices are yet to be defined.

## Measuring our success

As an initial target is to gather 1500 contributors in first quarter.

## FAQ

- Where to see the current status (features set) of Beta program?

**TODO: Those pages**

- Is only feature in Beta stage are aligned candidates for this program? What about Experimental?

Not only Beta features can participate in Beta program. Regardless the naming we also glad to use Experimental as well.

- Can paid features apply for Beta?

Yes, as each Beta program contributor receives Ultimate license for 1 month and more in case if they are active.

- What about features that are under feature flags? Can we add them into Beta?

That is the reason why do we ask Group ID that contributor will use for Beta testing. We will enable feature flag for this group in this case.
